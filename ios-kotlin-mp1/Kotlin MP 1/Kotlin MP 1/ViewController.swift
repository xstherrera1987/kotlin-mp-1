//
//  ViewController.swift
//  Kotlin MP 1
//
//  Created by Joe Herrera on 8/19/20.
//  Copyright © 2020 Joe Herrera. All rights reserved.
//

import UIKit
import SharedCode

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 21))
        label.center = CGPoint(x: 200, y: 32)
        label.textAlignment = .center
        label.font = label.font.withSize(24)
        
        let platformName = Platform().name // TODO: improve API of platform object
        label.text = "Kotlin MP 1 (\(platformName))"
        view.addSubview(label)
        
        val mealService = MealServiceIOS()
//        mealService.getCategoriesWithCompletionHandler
    }
}
