package com.jherrera.sharedCode.platform

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


public actual fun platformName(): String = "android"

public class MealServiceAndroid: BaseMealService() {

    override suspend fun getCategories(callback: Callback<CategoriesResponse>) {
        GlobalScope.launch(Dispatchers.Main) {
            val categories = withContext(Dispatchers.Default) {
                getCategories()
            }

            callback(categories)
        }
    }
}
