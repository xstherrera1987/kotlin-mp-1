package com.jherrera.sharedCode.http

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*

public actual fun httpClient(): HttpClient = HttpClient(Android) {
    install(JsonFeature) {
        serializer = KotlinxSerializer()
    }
}
