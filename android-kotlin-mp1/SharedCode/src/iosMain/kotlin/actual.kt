package com.jherrera.sharedCode.platform

import platform.UIKit.UIDevice

public actual fun platformName(): String = "${UIDevice.currentDevice.systemName()} ${UIDevice.currentDevice.systemVersion}"

public class MealServiceIOS: BaseMealService() {

}
