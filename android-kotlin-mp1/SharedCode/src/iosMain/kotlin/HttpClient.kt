package com.jherrera.sharedCode.http

import io.ktor.client.*
import io.ktor.client.engine.ios.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*

public actual fun httpClient(): HttpClient = HttpClient(Ios) {
    install(JsonFeature) {
        serializer = KotlinxSerializer()
    }
}
