package com.jherrera.sharedCode.http

import io.ktor.client.*

public expect fun httpClient(): HttpClient

public class Client {
    public fun get(): HttpClient = httpClient()
}
