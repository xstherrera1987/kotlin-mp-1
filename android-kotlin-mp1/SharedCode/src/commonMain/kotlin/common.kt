package com.jherrera.sharedCode.platform

import com.jherrera.sharedCode.http.Client
import io.ktor.client.request.*
import kotlinx.serialization.Serializable

public expect fun platformName(): String

// TODO: use new library features in Kotlin 1.4
// eg: private by default, deliberate exposure of data
public object Platform {
    public val name: String get() = platformName()
}

public class DeviceOfflineError: Exception("device is offline")

public interface IMealService {
    // https://www.themealdb.com/api/json/v1/1/categories.php
    public suspend fun getCategories(): CategoriesResponse

    public suspend fun getCategories(callback: Callback<CategoriesResponse>)

    /*

    // https://www.themealdb.com/api/json/v1/1/search.php?s=Arrabiata
    fun search(name: String)

    // https://www.themealdb.com/api/json/v1/1/lookup.php?i=52772
    fun mealDetails(id: Long)

    // https://www.themealdb.com/api/json/v1/1/filter.php?c=Seafood
    fun foodByCategory(category: String)

    // https://www.themealdb.com/images/media/meals/llcbn01574260722.jpg/preview
    fun imagePreview()

    // https://www.themealdb.com/images/ingredients/Lime.png
    fun ingredientImage()

     */
}

public fun interface Callback<T> {
    public operator fun invoke(data: T)
}

@Serializable
public data class CategoriesResponse(val categories: List<FoodCategory>)

@Serializable
public data class FoodCategory(
    val idCategory: Long,
    val strCategory: String,
    val strCategoryThumb: String, // url
    val strCategoryDescription: String,
)

public abstract class BaseMealService: IMealService {
    private val client get() = Client().get()

    override suspend fun getCategories(): CategoriesResponse =
        client.get<CategoriesResponse>("https://www.themealdb.com/api/json/v1/1/categories.php")

    override suspend fun getCategories(callback: Callback<CategoriesResponse>): Unit = callback(getCategories())
}
