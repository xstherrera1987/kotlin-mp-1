package com.jherrera.kotlinmp1

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.*
import com.airbnb.epoxy.preload.Preloadable
import com.jherrera.sharedCode.platform.FoodCategory

class FoodCategoryController : TypedEpoxyController<List<FoodCategory>>() {

    override fun buildModels(categories: List<FoodCategory>) {
        categories.forEach { add(
            FoodCategoryPreloadModel_()
                .id(it.idCategory)
                .title(it.strCategory)
                .description(it.strCategoryDescription)
                .thumbnailUrl(it.strCategoryThumb)
        )}
    }
}

@EpoxyModelClass(layout = R.layout.food_category)
abstract class FoodCategoryPreloadModel : EpoxyModelWithHolder<FoodCategoryPreloadModel.Holder>() {

    @EpoxyAttribute var title: String? = null
    @EpoxyAttribute var description: String? = null
    @EpoxyAttribute var thumbnailUrl: String? = null

    override fun bind(holder: Holder) {
        log("bind holder: $title")

        holder.run {
            titleView?.text = title
            descriptionView?.text = description
        }
    }

    class Holder: EpoxyHolder(), Preloadable {
        var thumbnail: ImageView? = null
        var titleView: TextView? = null
        var descriptionView: TextView? = null

        override fun bindView(itemView: View) {
            thumbnail = itemView.findViewById(R.id.categoryThumbnail)

            viewsToPreload.add(thumbnail!!)

            titleView = itemView.findViewById(R.id.categoryTitle)
            descriptionView = itemView.findViewById(R.id.categoryDescription)

            log("bind view: $itemView")
        }

        override val viewsToPreload: MutableList<View> = mutableListOf()
    }
}
