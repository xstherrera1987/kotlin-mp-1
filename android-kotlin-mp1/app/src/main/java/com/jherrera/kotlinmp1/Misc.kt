package com.jherrera.kotlinmp1

import android.util.Log

fun Any.log(msg: String) = Log.w(javaClass.simpleName, msg)
