@EpoxyDataBindingLayouts({
    R.layout.food_category
})

@PackageModelViewConfig(rClass = R.class)
package com.jherrera.kotlinmp1;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;
import com.airbnb.epoxy.PackageModelViewConfig;
