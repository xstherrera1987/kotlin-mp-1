package com.jherrera.kotlinmp1

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import com.airbnb.epoxy.addGlidePreloader
import com.airbnb.epoxy.glidePreloader
import com.bumptech.glide.Glide
import com.jherrera.sharedCode.platform.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope {
  override val coroutineContext: CoroutineContext get() = Job()

  private val recyclerView: EpoxyRecyclerView get() = epoxyRecyclerView
  private val mealService: IMealService = MealServiceAndroid()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = "$title (${Platform.name} ${android.os.Build.VERSION.RELEASE})"

    setContentView(R.layout.activity_main)

    val llm = LinearLayoutManager(this)
    llm.orientation = LinearLayoutManager.VERTICAL
    recyclerView.layoutManager = llm

    recyclerView.addItemDecoration(
      DividerItemDecoration(
        recyclerView.context,
        LinearLayoutManager.VERTICAL
      )
    )

    recyclerView.addGlidePreloader(
      glideRequestManager,
      preloader = glidePreloader { glideRequestManager, epoxyModel: FoodCategoryPreloadModel_, viewData ->
        log("glidePreloader for ${epoxyModel.title()}, url=${epoxyModel.thumbnailUrl()}")

        glideRequestManager
          .load(epoxyModel.thumbnailUrl())
          .centerCrop()
      }
    )

    loadAndRenderCategories()
  }

  private fun loadAndRenderCategories() {
    launch(Dispatchers.Main) {
      val categories = withContext(Dispatchers.Default) {
        log("querying food categories api")
        mealService.getCategories().categories
      }

      renderCategories(categories)
    }
  }

  private fun loadAndRenderCategoriesWithCallback() {


    launch(Dispatchers.Default) {
      val categories = withContext(Dispatchers.Default) {
        log("querying food categories api")
        mealService.getCategories().categories
      }

      mealService.getCategories {
        launch(Dispatchers.Main) {
          renderCategories(categories)
        }
      }
    }
  }

  private fun renderCategories(categories: List<FoodCategory>) {
    log("render categories (${categories.size})")
//    epoxyRecyclerView.withModels {
//      categories.forEach {
//        log(it.toString())
//
////        foodCategory {
////          id(it.idCategory)
////          title(it.strCategory)
////          description(it.strCategoryDescription)
////        }
//
//        FoodCategoryPreloadModel_()
//          .id(it.idCategory)
//          .title(it.strCategory)
//          .description(it.strCategoryDescription)
//          .thumbnailUrl(it.strCategoryThumb)
//      }
//    }

    val controller = FoodCategoryController()
    controller.setData(categories)
    epoxyRecyclerView.adapter = controller.adapter
  }

  private val glideRequestManager by lazy { Glide.with(this) }
  private fun log(msg: String) = Log.w(javaClass.simpleName, msg)
}
